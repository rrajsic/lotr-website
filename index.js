$('.carousel').carousel({
    interval: 2000
  });

  // Get the modal
var modal = document.getElementById("lotr1modal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img_banner1 = document.getElementById("img-banner-1");
var img_banner2 = document.getElementById("img-banner-2");
var img_banner3 = document.getElementById("img-banner-3");
var img_lotr1 = document.getElementById("img-lotr1");
var img_lotr2 = document.getElementById("img-lotr2");
var img_lotr3_1 = document.getElementById("img-lotr3-1");
var img_lotr3_2 = document.getElementById("img-lotr3-2");
var img_lotr3_3 = document.getElementById("img-lotr3-3");
var img_lotr3_4 = document.getElementById("img-lotr3-4");
var img_ivan = document.getElementById("img-ivan");
var img_robin = document.getElementById("img-robin");
var modalImg = document.getElementById("lotr1imgmodal");
var captionText = document.getElementById("caption");
img_banner1.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_banner2.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "images/banner6.jpg";
  captionText.innerHTML = this.alt;
}
img_banner3.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "images/ring2.jpg";
  captionText.innerHTML = this.alt;
}
img_lotr1.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

img_lotr2.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_lotr3_1.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_lotr3_2.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_lotr3_3.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_lotr3_4.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img_ivan.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "images/ivan_original.jpg";
  captionText.innerHTML = this.alt;
}
img_robin.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "images/robin_original.jpg";
  captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
} 